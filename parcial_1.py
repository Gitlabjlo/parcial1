import sqlite3
from sqlite3 import Error

try:
    con = sqlite3.connect('inventario.db')
    print("Conexion Exitosa")
except Error:
    print ("Conexion Fallida")

try:
    cur = con.cursor()
    cur.execute("CREATE TABLE IF NOT EXISTS Herramientas(id integer PRIMARY KEY, nombre text, cantidad int, precio float)")
    print("Creacion exitosa")
except Error:
    print("Error en creacion")

try:
   cur = con.cursor()
   cur.execute("INSERT INTO Herramientas VALUES(1, 'Martillo', '10', '5.00')")
   cur.execute("INSERT INTO Herramientas VALUES(2, 'Clavos grandes', '100', '0.89')")
   cur.execute("INSERT INTO Herramientas VALUES(3, 'Bloques', '500', '1.10')")
   cur.execute("INSERT INTO Herramientas VALUES(4, 'Silicon', '25', '3.99')")
   con.commit()
   print("Insercion exitosa")
except Error:
   print("Error en insercion")



def producto_nuevo (id, nombre, cantidad, precio):
    try:
        cur = con.cursor()
        cur.execute('''INSERT INTO Herramientas VALUES(?, ?, ?,?)''', (id, nombre, cantidad, precio))
        con.commit()
        print("Insercion exitosa")
    except Error:
        print("Error en insercion. Id existente")
        con.close()

def modificar_producto(cantidad, precio, id):
    try:
       cur = con.cursor()
       cur.execute('UPDATE Herramientas SET cantidad = ?, precio = ? WHERE id = ?',(cantidad, precio,id))
       con.commit()
       print("Actualizacion exitosa")
    except Error:
       print("Error al actualizar")
       con.close()
       
def borrar_producto(id):
    try:
       cur = con.cursor()
       cur.execute('DELETE FROM Herramientas WHERE id = %s' %id)
       con.commit()
       print("Producto Eliminado")
    except Error:
       print("Error al eliminar")
       con.close()

def mostrar():
    try:
       cur = con.cursor()
       cur.execute('SELECT * FROM Herramientas')
       herramienta = cur.fetchall()
       for Herramientas in herramienta:
            print(Herramientas)
    except Error:
       print("Error al traer datos")
       con.close

def buscar_producto(id):
     try:
       cur = con.cursor()
       cur.execute('SELECT * FROM Herramientas WHERE id=%s' %id)
       producto = cur.fetchall()
       for Herramientas in producto:
           print("Detalles del producto encontrado: ", producto)
     except Error:
       print("Error al traer datos")
       con.close()

import os
def menu():
    #os.system("cls")
    print ()
    print ()
    print ("SISTEMA DE INVENTARIO DE HERRAMIENTAS DE CONSTRUCCION")
    print ("Aplicación de línea de comandos que permita registro, búsqueda, edición y eliminación de artículos dentro de un sistema de inventario.")
    print ("Selecciona una opcion del siguiente menu:")
    print ("1) AGREGA UNA HERRAMIENTA")
    print ("2) EDITA UN HERRAMIENTA")
    print ("3) ELIMINA UNA HERRAMIENTA")
    print ("4) VER INVENTARIO DE HERRAMIENTAS")
    print ("5) BUSCAR UNA HERRAMIENTA")
    print ("6) SALIR")
    print ()
    

while True:
    menu()

# Leemos lo que ingresa el usuario
    opcion = input("Selecciona :")
    print ()
    if opcion == "1":
        print ("Seleccionaste la opcion que agrega un nuevo producto al inventario.")
        id = int(input("ID: "))
        nombre = input("Herramienta: ")
        cantidad = input("Cantiad: ")
        precio = input("Precio: ")
        producto_nuevo(id, nombre, cantidad, precio)
        
    elif opcion == "2":
        print ("Seleccionaste la opcion que modifica un producto del inventario.")
        id = int (input ("Ingresa un ID: "))
        cantidad = input ("Ingresa la cantidad actualizada: ")
        precio = input ("Ingresa el precio actualizado: ")
        modificar_producto(cantidad, precio, id)
        
    elif opcion == "3":
        print ("Seleccionaste la opcion que borra una Herramienta")
        id = int (input ("Ingresa el ID del producto que deseas eliminar: ")) 
        borrar_producto(id)
                   
    elif opcion == "4":
        print ("Seleccionaste la opcion que nos muestra todos las Herramientas en el inventario.")
        mostrar()
                
    elif opcion == "5":
        print ("Seleccionaste la opcion que nos busca una herramienta por su ID")
        id = int (input ("Ingresa un ID del producto que quieras saber su inventario en Stock: "))
        buscar_producto(id)
        
    elif opcion == "6":
        print ("Tengo tu inventario en orden, vuelve pronto.")
        exit()
    else:
        print("opcion invalida.")

   
